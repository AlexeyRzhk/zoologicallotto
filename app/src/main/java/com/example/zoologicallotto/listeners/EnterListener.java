package com.example.zoologicallotto.listeners;

import android.content.Context;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.example.zoologicallotto.activities.QuestionCreationActivity;

public class EnterListener implements View.OnKeyListener{
    Context context;
    View view;

    public EnterListener(Context context, View view) {
        this.context = context;
        this.view = view;
    }

    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(QuestionCreationActivity.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            return true;
        }
        return false;
    }
}
