package com.example.zoologicallotto;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.net.Uri;
import android.util.DisplayMetrics;

import com.example.zoologicallotto.storages.Pictures;

import java.io.FileDescriptor;
import java.net.URI;

public class Picture {
    private String path;
    private int pictureID;
    private Integer uuid;
    private static int number = 0;

    public Picture(){
        uuid = number;
        this.pictureID = R.drawable.ic_launcher_background;
        number++;
    }

    public Picture(int pictureID){
        uuid = number;
        this.pictureID = pictureID;
        number++;
    }

    public Picture(String path){
        uuid = number;
        this.path = path;
        number++;
    }

    public Picture(String path, Integer pictureID, int uuid) {
        this.path = path;
        this.pictureID = pictureID;
        this.uuid = uuid;
        number++;
    }

    public static void setNumber(int number) {
        Picture.number = number;
    }

    public String getPath() {
        return path;
    }

    public int getPictureID() {
        return pictureID;
    }

    public Bitmap getPicture(Context context){
        if(pictureID != 0){
            Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), pictureID);
            return bitmap;
        }
        return getScaledBitmap(path, 100, 100, context);
    }

    public Integer getUuid() {
        return uuid;
    }

    private static Bitmap getScaledBitmap(String path, int destWidth, int destHeight, Context context) {
        try {
// Чтение размеров изображения на диске
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(context.getContentResolver().openInputStream(Uri.parse(path)), new Rect(), options);

            //BitmapFactory.decodeFileDescriptor(context.getContentResolver().openFileDescriptor(Uri.parse(path), "r").getFileDescriptor());
            int i = Pictures.getInstance().getPictures().get(0).getPicture(context).getHeight();
            float srcWidth = options.outWidth;
            float srcHeight = options.outHeight;
            destWidth = i;
            destHeight = i;
// Вычисление степени масштабирования
            int inSampleSize = 1;
            if (srcHeight > destHeight || srcWidth > destWidth) {
                if (srcWidth > srcHeight) {
                    inSampleSize = Math.round(srcHeight / destHeight);
                } else {
                    inSampleSize = Math.round(srcWidth / destWidth);
                }
            }
            options = new BitmapFactory.Options();
            options.inSampleSize = inSampleSize;
// Чтение данных и создание итогового изображения
            Bitmap bmp = BitmapFactory.decodeStream(context.getContentResolver().openInputStream(Uri.parse(path)), new Rect(), options);
            bmp = Bitmap.createScaledBitmap(bmp, destWidth, destHeight, true);
            return bmp;
        }catch (Exception e){

        }
        return null;
    }
}
