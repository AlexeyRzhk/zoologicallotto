package com.example.zoologicallotto.activities;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.zoologicallotto.Category;
import com.example.zoologicallotto.Question;
import com.example.zoologicallotto.storages.Pictures;
import com.example.zoologicallotto.storages.QuestionStorage;
import com.example.zoologicallotto.R;
import com.example.zoologicallotto.tasks.CategoryImageLoadTask;
import com.example.zoologicallotto.tasks.QuestionImageLoadTask;

import java.util.List;

public class QuestionDeleteActivity extends AppCompatActivity {
    private RecyclerView categoriesRecyclerView;
    private RecyclerView questionRecyclerView;
    private Button backButton;
    private QuestionStorage storage;
    private Category selectedCategory;
    private int selectedPosition = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question_delete_layout);
        storage = QuestionStorage.getStorage(this);
        selectedCategory = storage.getCategories().get(0);
        categoriesRecyclerView = findViewById(R.id.question_delete_category_recycler_view);
        categoriesRecyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        categoriesRecyclerView.setAdapter(new CategoryAdapter(storage.getNotBaseCategories(), this));
        questionRecyclerView = findViewById(R.id.question_delete_question_recycler_view);
        //questionRecyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        questionRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        questionRecyclerView.setAdapter(new QuestionAdapter(storage.getNotBaseQuestionsInCategory(selectedCategory), this));
        backButton = findViewById(R.id.question_delete_back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private class CategoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView text;
        private ImageView image;
        private Category category;
        private Context context;
        private CategoryAdapter adapter;

        public CategoryHolder(@NonNull View itemView, Context context, CategoryAdapter adapter) {
            super(itemView);
            this.context = context;
            image =  itemView.findViewById(R.id.category_item_image);
            text = itemView.findViewById(R.id.category_item_text);
            this.adapter = adapter;
            itemView.setOnClickListener(this);
        }

        public void bindCategory(Category category){
            this.category = category;
            image.setImageBitmap(Pictures.getInstance().getPictures().get(62).getPicture(getApplicationContext()));
            new CategoryImageLoadTask(this, category.getPicture(), context).execute();
            text.setText(category.getCategoryName());
        }

        @Override
        public void onClick(View v) {
            selectedCategory = category;
            questionRecyclerView.setAdapter(new QuestionAdapter(storage.getNotBaseQuestionsInCategory(selectedCategory), QuestionDeleteActivity.this));
            adapter.notifyItemChanged(selectedPosition);
            adapter.notifyItemChanged(getAdapterPosition());
            selectedPosition = getAdapterPosition();
        }
    }

    private class CategoryAdapter extends RecyclerView.Adapter<CategoryHolder> {
        private List<Category> categories;
        private Context context;

        public CategoryAdapter(List<Category> category, Context context) {
            this.categories = category;
            this.context = context;
        }

        @Override
        public CategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = getLayoutInflater();
            View v = inflater.inflate(R.layout.category_item, parent, false);
            return new CategoryHolder(v, context, this);
        }

        @Override
        public void onBindViewHolder(@NonNull CategoryHolder holder, int position) {
            Category category = categories.get(position);
            holder.bindCategory(category);
            if(category.equals(selectedCategory)) {
                holder.itemView.setBackground(context.getDrawable(R.drawable.category_selected_item_border));
            }
            else{
                holder.itemView.setBackground(context.getDrawable(R.drawable.category_item_border));
            }
        }

        @Override
        public int getItemCount() {
            return categories.size();
        }
    }

    private class QuestionHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private Question question;
        private Context context;
        private ImageView image;


        public QuestionHolder(@NonNull View itemView, Context context) {
            super(itemView);
            this.context = context;
            image = itemView.findViewById(R.id.question_item_image);
            itemView.setOnClickListener(this);
        }

        public void bindQuestion(Question question){
            this.question = question;
            image.setImageBitmap(Pictures.getInstance().getPictures().get(84).getPicture(getApplicationContext()));
            new QuestionImageLoadTask(this, question.getPicture(), context).execute();
        }

        @Override
        public void onClick(View v) {
            processClick(this);
        }
    }

    private class QuestionAdapter extends RecyclerView.Adapter<QuestionHolder> {
        private List<Question> questions;
        private Context context;

        public QuestionAdapter(List<Question> questions, Context context) {
            this.questions = questions;
            this.context = context;
        }

        @Override
        public QuestionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = getLayoutInflater();
            View v = inflater.inflate(R.layout.question_item, parent, false);
            return new QuestionHolder(v, context);
        }

        public void onBindViewHolder(@NonNull QuestionHolder holder, int position) {
            Question question = questions.get(position);
            holder.bindQuestion(question);
        }

        @Override
        public int getItemCount() {
            return questions.size();
        }
    }
    private void updateSelectedCategory(){
        questionRecyclerView.setAdapter(new QuestionAdapter(selectedCategory.getQuestions(), this));
    }

    private class ClickThread extends Thread{
        private QuestionHolder holder;

        @Override
        public void run() {
            try{
                Thread.sleep(3000);
            }
            catch (Exception e){

            }
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    processClick(holder);
                }
            });
        }
        public ClickThread(QuestionHolder holder) {
            this.holder = holder;
        }
    }

    private void processClick(final QuestionHolder holder){
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedCategory.deleteQuestion(holder.question);
                storage.delete(holder.question);
                updateSelectedCategory();
            }
        });
        Toast.makeText(QuestionDeleteActivity.this, R.string.click_again, Toast.LENGTH_SHORT).show();
        new ClickThread(holder).start();
    }
}
