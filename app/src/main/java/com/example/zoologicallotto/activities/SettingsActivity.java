package com.example.zoologicallotto.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.example.zoologicallotto.R;
import com.example.zoologicallotto.dialogs.CustomDialog;
import com.example.zoologicallotto.storages.QuestionStorage;

public class SettingsActivity extends AppCompatActivity {

    private static final int REQUEST = 1;

    private Button backButton;
    private Button categoryCreateButton;
    private Button questionCreateButton;
    private Button categoryDeleteButton;
    private Button questionDeleteButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_layout);
        backButton = findViewById(R.id.settings_back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        categoryCreateButton = findViewById(R.id.settings_category_create_button);
        categoryCreateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ActivityCompat.checkSelfPermission(SettingsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(SettingsActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST);
                }
                else {
                    categoryCreateButton.setEnabled(false);
                    Intent intent = new Intent(SettingsActivity.this, CategoryCreationActivity.class);
                    startActivity(intent);
                }
            }
        });
        questionCreateButton = findViewById(R.id.settings_question_create_button);
        questionCreateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ActivityCompat.checkSelfPermission(SettingsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(SettingsActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST);
                }
                else {
                    if (QuestionStorage.getStorage(SettingsActivity.this).getNotBaseCategories().size() > 0) {
                        questionCreateButton.setEnabled(false);
                        Intent intent = new Intent(SettingsActivity.this, QuestionCreationActivity.class);
                        startActivity(intent);
                    } else {
                        //Toast.makeText(SettingsActivity.this, "Нет категорий, создайте новую", Toast.LENGTH_SHORT).show();
                        CustomDialog dialog = new CustomDialog("Создайте категорию");
                        dialog.show(getSupportFragmentManager(), "custom");
                    }
                }
            }
        });
        categoryDeleteButton = findViewById(R.id.settings_category_delete_button);
        categoryDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ActivityCompat.checkSelfPermission(SettingsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(SettingsActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST);
                }
                else {
                    if (QuestionStorage.getStorage(SettingsActivity.this).getNotBaseCategories().size() > 0) {
                        categoryDeleteButton.setEnabled(false);
                        Intent intent = new Intent(SettingsActivity.this, CategoryDeleteActivity.class);
                        startActivity(intent);
                    } else {
                        CustomDialog dialog = new CustomDialog("Нет категорий для удаления");
                        dialog.show(getSupportFragmentManager(), "custom");
                    }
                }
            }
        });
        questionDeleteButton = findViewById(R.id.settings_question_delete_button);
        questionDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ActivityCompat.checkSelfPermission(SettingsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(SettingsActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST);
                }
                else {
                    if (QuestionStorage.getStorage(SettingsActivity.this).getNumberOfUserQuestions() > 0) {
                        questionDeleteButton.setEnabled(false);
                        Intent intent = new Intent(SettingsActivity.this, QuestionDeleteActivity.class);
                        startActivity(intent);
                    } else {
                        CustomDialog dialog = new CustomDialog("Нет вопросов для удаления");
                        dialog.show(getSupportFragmentManager(), "custom");
                    }
                }
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        categoryCreateButton.setEnabled(true);
        questionCreateButton.setEnabled(true);
        categoryDeleteButton.setEnabled(true);
        questionDeleteButton.setEnabled(true);
    }


}
