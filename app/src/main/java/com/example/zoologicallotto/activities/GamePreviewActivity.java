package com.example.zoologicallotto.activities;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.zoologicallotto.storages.GameSettings;
import com.example.zoologicallotto.Question;
import com.example.zoologicallotto.R;

import java.util.List;

public class GamePreviewActivity extends AppCompatActivity {
    private ImageView imageView;
    private EditText editText;
    private TextView textView;
    private TextView hintTextView;

    private Button nextButton;
    private Button prevButton;
    private GameSettings settings;
    private List<Question> questions;
    private int currentQuestion;
    private TextView categoryName;
    private Context context;
    private String language;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_layout);
        context = getApplicationContext();
        settings = GameSettings.getSettings(this);
        language = settings.getChosenLanguage();
        questions = settings.getChosenCategory().getQuestions();
        hintTextView = findViewById(R.id.game_text_view_hint);
        hintTextView.setText("");
        categoryName = findViewById(R.id.game_category_name);
        categoryName.setText(GameSettings.getSettings(this).getChosenCategory().getCategoryName());
        imageView = findViewById(R.id.game_picture);
        editText = findViewById(R.id.game_text_edit);
        textView = findViewById(R.id.game_text_view);
        nextButton = findViewById(R.id.game_next_button);
        nextButton.setOnClickListener(new NextButtonListener());
        prevButton = findViewById(R.id.game_prev_button);
        prevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(currentQuestion > 0) {
                    currentQuestion--;
                    updateQuestion();
                }
            }
        });
        editText.setEnabled(false);
        updateQuestion();
    }

    private void updateQuestion(){
        if(questions.size() > 0) {
            imageView.setImageBitmap(questions.get(currentQuestion).getPicture(this));
            textView.setText(String.format("вопрос %s из %s", currentQuestion + 1, questions.size()));
            if(language.equals(getApplicationContext().getString(R.string.russian))) {
                editText.setText(questions.get(currentQuestion).getRussianAnswer());
            }
            else if(language.equals(getApplicationContext().getString(R.string.english))) {
                editText.setText(String.format("%s\n%s", questions.get(currentQuestion).getRussianAnswer(), questions.get(currentQuestion).getEnglishAnswer()));
            }
            else if(language.equals(getApplicationContext().getString(R.string.deutsch))){
                    editText.setText(String.format("%s\n%s", questions.get(currentQuestion).getRussianAnswer(), questions.get(currentQuestion).getDeutschAnswer()));
            }
            if(currentQuestion == questions.size() - 1){
                nextButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });
                nextButton.setText(R.string.endgame);
            }
            else if(currentQuestion < questions.size() - 1 && nextButton.getText().toString().equals(context.getString(R.string.endgame))){
                nextButton.setOnClickListener(new NextButtonListener());
                nextButton.setText(R.string.next_question);
            }
        }
    }

    private class NextButtonListener implements View.OnClickListener{
        @Override
        public void onClick(View view) {
            currentQuestion++;
            updateQuestion();
        }
    }
}
