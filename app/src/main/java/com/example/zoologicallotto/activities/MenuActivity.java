package com.example.zoologicallotto.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.zoologicallotto.storages.GameSettings;
import com.example.zoologicallotto.storages.QuestionStorage;
import com.example.zoologicallotto.R;

public class MenuActivity extends AppCompatActivity {

    //private ImageView imageView;
    private TextView textView;
    private Button settingsButton;
    private Button startButton;
    //private Button previewButton;


    @Override
    protected void onResume() {
        super.onResume();
        settingsButton.setEnabled(true);
        startButton.setEnabled(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_layout);
        QuestionStorage.getStorage(getApplicationContext());
        //imageView = findViewById(R.id.menu_image);
        textView = findViewById(R.id.menu_language);
        settingsButton = findViewById(R.id.menu_settings_button);
        startButton = findViewById(R.id.menu_start_game_button);
        //previewButton = findViewById(R.id.menu_start_preview_button);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                settingsButton.setEnabled(false);
                Intent intent = new Intent(MenuActivity.this, SettingsActivity.class);
                startActivity(intent);
            }
        });
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startButton.setEnabled(false);
                Intent intent = new Intent(MenuActivity.this, ChooseCategoryActivity.class);
                startActivity(intent);
            }
        });

    }
}
