package com.example.zoologicallotto.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.zoologicallotto.storages.GameSettings;
import com.example.zoologicallotto.Question;
import com.example.zoologicallotto.R;

import java.util.HashMap;
import java.util.List;

public class GameActivity extends AppCompatActivity {
    private ImageView imageView;
    private EditText editText;
    private TextView textView;
    private TextView categoryName;
    private Button nextButton;
    private Button prevButton;
    //private TextView hintTextView;

    private GameSettings settings;
    private List<Question> questions;
    private int currentQuestion;
    private HashMap<Integer, String> answers;
    private Context context;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_layout);
        answers = new HashMap<>();
        context = getApplicationContext();
        settings = GameSettings.getSettings(this);
        questions = settings.getChosenCategory().getQuestions();
        for(int i = currentQuestion;i < questions.size();i++){
            answers.put(i, "");
        }
        categoryName = findViewById(R.id.game_category_name);
        categoryName.setText(GameSettings.getSettings(this).getChosenCategory().getCategoryName());
        imageView = findViewById(R.id.game_picture);
        editText = findViewById(R.id.game_text_edit);
        editText.setText("");
        textView = findViewById(R.id.game_text_view);
        nextButton = findViewById(R.id.game_next_button);
        nextButton.setOnClickListener(new NextButtonListener());
        prevButton = findViewById(R.id.game_prev_button);
        prevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(currentQuestion > 0) {
                    if (answers.get(currentQuestion).equals("") || !editText.getText().equals("")) {
                        answers.put(currentQuestion, editText.getText().toString());
                        currentQuestion--;
                        editText.setText("");
                        updateQuestion();
                    }
                }
            }
        });
        updateQuestion();
    }

    private void updateQuestion(){
        if(questions.size() > 0) {
            imageView.setImageBitmap(questions.get(currentQuestion).getPicture(this));
            textView.setText(String.format("вопрос %s из %s", currentQuestion + 1, questions.size()));
            if(currentQuestion == questions.size() - 1){
                nextButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(settings.isGameType()) {
                            nextButton.setEnabled(false);
                            answers.put(currentQuestion, editText.getText().toString());
                            Intent intent = new Intent(GameActivity.this, EndGameActivity.class);
                            intent.putExtra("answers", answers);
                            startActivity(intent);
                        }
                        finish();
                    }
                });
                nextButton.setText(R.string.endgame);
            }
            else if(currentQuestion < questions.size() - 1 && nextButton.getText().toString().equals(context.getString(R.string.endgame))){
                nextButton.setOnClickListener(new NextButtonListener());
                nextButton.setText(R.string.next_question);
            }
        }
    }

    private class NextButtonListener implements View.OnClickListener{
        @Override
        public void onClick(View view) {
            if(answers.get(currentQuestion).equals("") || !editText.getText().equals("")) {
                answers.put(currentQuestion, editText.getText().toString());
                currentQuestion++;
                editText.setText("");
                updateQuestion();
            }
        }
    }
}
