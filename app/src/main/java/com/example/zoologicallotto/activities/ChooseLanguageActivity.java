package com.example.zoologicallotto.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.zoologicallotto.R;
import com.example.zoologicallotto.storages.GameSettings;

public class ChooseLanguageActivity extends AppCompatActivity {
    private Button ruButton;
    private Button enButton;
    private Button deButton;
    private Button startGameButton;
    private Button startPreviewButton;
    private TextView currentLanguageView;
    private GameSettings settings;
    private String currentLanguage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choose_language_layout);
        settings = GameSettings.getSettings(this);
        ruButton = findViewById(R.id.choose_language_button_russian);
        ruButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                settings.setChosenLanguage(getApplicationContext().getString(R.string.russian));
                currentLanguage = getString(R.string.current_language) + settings.getChosenLanguage();
                currentLanguageView.setText(currentLanguage);
            }
        });
        enButton = findViewById(R.id.choose_language_button_english);
        enButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                settings.setChosenLanguage(getApplicationContext().getString(R.string.english));
                currentLanguage = getString(R.string.current_language) + settings.getChosenLanguage();
                currentLanguageView.setText(currentLanguage);
            }
        });
        deButton = findViewById(R.id.choose_language_button_deutsch);
        deButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                settings.setChosenLanguage(getApplicationContext().getString(R.string.deutsch));
                currentLanguage = getString(R.string.current_language) + settings.getChosenLanguage();
                currentLanguageView.setText(currentLanguage);
            }
        });
        startGameButton = findViewById(R.id.choose_language_start_game_button);
        startGameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GameSettings.getSettings(getApplicationContext()).setGameType(true);
                Intent intent = new Intent(ChooseLanguageActivity.this, GameActivity.class);
                startActivity(intent);
                finish();
            }
        });
        startPreviewButton = findViewById(R.id.choose_language_start_preview_button);
        startPreviewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GameSettings.getSettings(getApplicationContext()).setGameType(false);
                Intent intent = new Intent(ChooseLanguageActivity.this, GamePreviewActivity.class);
                startActivity(intent);
                finish();
            }
        });
        currentLanguageView = findViewById(R.id.choose_language_text_view);
        currentLanguage = getString(R.string.current_language) + settings.getChosenLanguage();
        currentLanguageView.setText(currentLanguage);
    }
}
