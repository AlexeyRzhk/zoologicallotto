package com.example.zoologicallotto.activities;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.zoologicallotto.Category;
import com.example.zoologicallotto.storages.Pictures;
import com.example.zoologicallotto.storages.QuestionStorage;
import com.example.zoologicallotto.R;
import com.example.zoologicallotto.tasks.CategoryImageLoadTask;

import java.util.List;

public class CategoryDeleteActivity extends AppCompatActivity {
    private RecyclerView categoriesRecyclerView;
    private Button backButton;
    private QuestionStorage storage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.category_delete_layout);
        storage = QuestionStorage.getStorage(this);
        categoriesRecyclerView = findViewById(R.id.category_delete_recycler_view);
        //categoriesRecyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        categoriesRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        categoriesRecyclerView.setAdapter(new CategoryAdapter(storage.getNotBaseCategories(), this));
        backButton = findViewById(R.id.category_delete_back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private class CategoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView text;
        private ImageView image;
        private Category category;
        private Context context;

        public CategoryHolder(@NonNull View itemView, Context context) {
            super(itemView);
            this.context = context;
            image =  itemView.findViewById(R.id.category_item_image);
            text = itemView.findViewById(R.id.category_item_text);
            itemView.setOnClickListener(this);
        }

        public void bindCategory(Category category){
            this.category = category;
            image.setImageBitmap(Pictures.getInstance().getPictures().get(62).getPicture(getApplicationContext()));
            new CategoryImageLoadTask(this, category.getPicture(), context).execute();
            text.setText(category.getCategoryName());
        }

        @Override
        public void onClick(View v) {
            processClick(this);

        }
    }

    private class CategoryAdapter extends RecyclerView.Adapter<CategoryHolder> {
        private List<Category> categories;
        private Context context;

        public CategoryAdapter(List<Category> category, Context context) {
            this.categories = category;
            this.context = context;
        }

        @Override
        public CategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = getLayoutInflater();
            View v = inflater.inflate(R.layout.category_item, parent, false);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(5,5,5,5);
            v.findViewById(R.id.category_item_layout).setLayoutParams(params);
            return new CategoryHolder(v, context);
        }

        @Override
        public void onBindViewHolder(@NonNull CategoryHolder holder, int position) {
            Category category = categories.get(position);
            holder.bindCategory(category);
        }

        @Override
        public int getItemCount() {
            return categories.size();
        }
    }


    private void update(){
        categoriesRecyclerView.setAdapter(new CategoryAdapter(storage.getNotBaseCategories(), this));
    }

    private class ClickThread extends Thread{
        CategoryHolder holder;
        @Override
        public void run() {
            try{
                Thread.sleep(3000);
            }catch(Exception e){

            }
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    processClick(holder);
                }
            });
        }

        public ClickThread(CategoryHolder holder) {
            this.holder = holder;
        }
    }

    private void processClick(final CategoryHolder holder){
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                storage.getCategories().remove(holder.category);
                storage.delete(holder.category);
                update();
            }
        });
        Toast.makeText(CategoryDeleteActivity.this, R.string.click_again, Toast.LENGTH_SHORT).show();
        new ClickThread(holder).start();

    }
}
