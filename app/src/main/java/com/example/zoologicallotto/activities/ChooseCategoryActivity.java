package com.example.zoologicallotto.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.zoologicallotto.Category;
import com.example.zoologicallotto.R;
import com.example.zoologicallotto.storages.GameSettings;
import com.example.zoologicallotto.storages.Pictures;
import com.example.zoologicallotto.storages.QuestionStorage;
import com.example.zoologicallotto.tasks.CategoryImageLoadTask;

import java.util.List;

public class ChooseCategoryActivity extends AppCompatActivity {

    private RecyclerView categoriesRecyclerView;
    private Button nextStageButton;
    private GameSettings settings;
    private TextView categoryNameTextView;
    private int selectedPosition = 0;
    private Category selectedCategory;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.choose_category_layout);
        settings = GameSettings.getSettings(this);
        selectedCategory = null;

        settings.setChosenCategory(QuestionStorage.getStorage(this).getCategories().get(0));
        selectedCategory = QuestionStorage.getStorage(this).getCategories().get(0);
        categoryNameTextView = findViewById(R.id.choose_category_name_text_view);
        String text = getString(R.string.category_choosed) + settings.getChosenCategory().getCategoryName();
        categoryNameTextView.setText(text);
        categoriesRecyclerView = findViewById(R.id.category_choose_recycler_view);
        categoriesRecyclerView.setLayoutManager(new GridLayoutManager(this,  3){});
        categoriesRecyclerView.setAdapter(new CategoryAdapter(QuestionStorage.getStorage(this).getCategories(), this));
        nextStageButton = findViewById(R.id.category_choose_next_button);
        nextStageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(settings.getChosenCategory().getQuestions().size() > 0) {
                    Intent intent = new Intent(ChooseCategoryActivity.this, ChooseLanguageActivity.class);
                    startActivity(intent);
                    finish();
                }
                else{
                    Toast.makeText(ChooseCategoryActivity.this, "В категории нет вопросов, выберите другую", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        categoriesRecyclerView.setAdapter(new CategoryAdapter(QuestionStorage.getStorage(this).getCategories(), this));
    }

    private class CategoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView text;
        private ImageView image;
        private Category category;
        private Context context;
        private CategoryAdapter adapter;

        public CategoryHolder(@NonNull View itemView, Context context, CategoryAdapter adapter) {
            super(itemView);
            this.context = context;
            image =  itemView.findViewById(R.id.category_item_image);
            text = itemView.findViewById(R.id.category_item_text);
            this.adapter = adapter;
            itemView.setOnClickListener(this);
        }

        public void bindCategory(Category category){
            this.category = category;
            image.setImageBitmap(Pictures.getInstance().getPictures().get(62).getPicture(getApplicationContext()));
            new CategoryImageLoadTask(this, category.getPicture(), context).execute();
            text.setText(category.getCategoryName());
        }

        @Override
        public void onClick(View v) {
            if(!category.equals(settings.getChosenCategory())) {
                settings.setChosenCategory(category);
                selectedCategory = category;
                adapter.notifyItemChanged(selectedPosition);
                adapter.notifyItemChanged(getAdapterPosition());
                selectedPosition = getAdapterPosition();
                String text = getString(R.string.category_choosed) + settings.getChosenCategory().getCategoryName();
                categoryNameTextView.setText(text);
            }
        }
    }

    private class CategoryAdapter extends RecyclerView.Adapter<CategoryHolder> {
        private List<Category> categories;
        private Context context;

        public CategoryAdapter(List<Category> category, Context context) {
            this.categories = category;
            this.context = context;
        }

        @Override
        public CategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = getLayoutInflater();
            View v = inflater.inflate(R.layout.category_item, parent, false);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(5,5,5,5);
            v.findViewById(R.id.category_item_layout).setLayoutParams(params);
            return new CategoryHolder(v, context, this);
        }

        @Override
        public void onBindViewHolder(@NonNull CategoryHolder holder, int position) {
            Category category = categories.get(position);
            holder.bindCategory(category);
            if(category.equals(selectedCategory)) {
                holder.itemView.setBackground(context.getDrawable(R.drawable.category_selected_item_border));
            }
            else{
                holder.itemView.setBackground(context.getDrawable(R.drawable.category_item_border));
            }
        }

        @Override
        public int getItemCount() {
            return categories.size();
        }
    }
}
