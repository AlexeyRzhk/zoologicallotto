package com.example.zoologicallotto.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.zoologicallotto.Category;
import com.example.zoologicallotto.listeners.EnterListener;
import com.example.zoologicallotto.Picture;
import com.example.zoologicallotto.storages.QuestionStorage;
import com.example.zoologicallotto.R;

public class CategoryCreationActivity extends AppCompatActivity {
    private static final int GALLERY_REQUEST = 1;
    private EditText categoryName;
    private ImageView categoryImage;
    private Button pickButton;
    private Button createButton;
    private Button backButton;
    private Picture image;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.category_creation_layout);
        final QuestionStorage storage = QuestionStorage.getStorage(CategoryCreationActivity.this);
        categoryName = findViewById(R.id.category_creation_category_name_editText);
        categoryName.setOnKeyListener(new EnterListener(this, categoryName));
        categoryImage = findViewById(R.id.category_creation_picture);
        pickButton = findViewById(R.id.category_creation_pick_button);
        pickButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, GALLERY_REQUEST);
            }
        });
        createButton = findViewById(R.id.category_creation_create_button);
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!categoryName.getText().toString().equals("") && image != null) {
                    Category category = new Category(categoryName.getText().toString(), image);
                    storage.getCategories().add(category);
                    storage.add(image);
                    storage.add(category);
                    finish();
                }
                else{
                    if(categoryName.getText().toString().equals("")) {
                        Toast.makeText(CategoryCreationActivity.this, "заполните название", Toast.LENGTH_SHORT).show();
                        categoryName.requestFocus();
                        InputMethodManager imm = (InputMethodManager)getSystemService(CategoryCreationActivity.INPUT_METHOD_SERVICE);
                        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,InputMethodManager.HIDE_IMPLICIT_ONLY);
                    }
                    else if (image == null){
                        Toast.makeText(CategoryCreationActivity.this, "выберите картинку", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });
        backButton = findViewById(R.id.category_creation_back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        categoryName.requestFocus();
        InputMethodManager imm = (InputMethodManager)getSystemService(this.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,InputMethodManager.HIDE_IMPLICIT_ONLY);

        }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch(requestCode) {
            case GALLERY_REQUEST:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                    try {
                        image = new Picture(selectedImage.toString());

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    categoryImage.setImageBitmap(image.getPicture(this));
                }

        }

    }
}
