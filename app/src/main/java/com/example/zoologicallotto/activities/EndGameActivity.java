package com.example.zoologicallotto.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.zoologicallotto.Category;
import com.example.zoologicallotto.storages.GameSettings;
import com.example.zoologicallotto.storages.Pictures;
import com.example.zoologicallotto.Question;
import com.example.zoologicallotto.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class EndGameActivity extends AppCompatActivity {
    private ImageView endGameImage;
    private TextView result;
    private Button backButton;
    private Button watchAnswersButton;
    private int numberOfRightAnswers;
    private GameSettings settings;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.endgame_layout);
        endGameImage = findViewById(R.id.endgame_picture);
        endGameImage.setImageBitmap(Pictures.getInstance().getPictures().get(26).getPicture(this));
        result = findViewById(R.id.endgame_text_view);
        backButton = findViewById(R.id.endgame_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        settings = GameSettings.getSettings(this);

        final Category questionsWithWrongAnswers = new Category("неправильные ответы");
        int numberOfQuestions = settings.getChosenCategory().getQuestions().size();
        HashMap<Integer, String> answers = (HashMap<Integer, String>) getIntent().getSerializableExtra("answers");

        for(int i = 0;i < numberOfQuestions;i++){
            List<Question> questions = settings.getChosenCategory().getQuestions();
            if(settings.getChosenLanguage().equals(getApplicationContext().getString(R.string.russian))){
                if (answers.get(i).toLowerCase().trim().equals(questions.get(i).getRussianAnswer().toLowerCase().trim())) {
                    numberOfRightAnswers++;
                }
                else{
                    questionsWithWrongAnswers.addQuestion(questions.get(i));
                }
            }
            else if(settings.getChosenLanguage().equals(getApplicationContext().getString(R.string.english))){
                if (answers.get(i).toLowerCase(Locale.ENGLISH).trim().equals(questions.get(i).getEnglishAnswer().toLowerCase(Locale.ENGLISH).trim())) {
                    numberOfRightAnswers++;
                }
                else{
                    questionsWithWrongAnswers.addQuestion(questions.get(i));
                }
            }
            else if(settings.getChosenLanguage().equals(getApplicationContext().getString(R.string.deutsch))){
                if (answers.get(i).toLowerCase(Locale.GERMANY).trim().equals(questions.get(i).getDeutschAnswer().toLowerCase(Locale.GERMANY).trim())) {
                    numberOfRightAnswers++;
                }
                else{
                    questionsWithWrongAnswers.addQuestion(questions.get(i));
                }
            }
        }


        result.setText(String.format("Вы правильно ответили на %s вопросов из %s.", numberOfRightAnswers, numberOfQuestions));

        watchAnswersButton = findViewById(R.id.endgame_answers_button);
        watchAnswersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                settings.setChosenCategory(questionsWithWrongAnswers);
                Intent intent = new Intent(EndGameActivity.this, GamePreviewActivity.class);
                startActivity(intent);
            }
        });
    }
}
