package com.example.zoologicallotto.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.zoologicallotto.Category;
import com.example.zoologicallotto.listeners.EnterListener;
import com.example.zoologicallotto.Picture;
import com.example.zoologicallotto.Question;
import com.example.zoologicallotto.storages.Pictures;
import com.example.zoologicallotto.storages.QuestionStorage;
import com.example.zoologicallotto.R;
import com.example.zoologicallotto.tasks.CategoryImageLoadTask;

import java.util.List;

public class QuestionCreationActivity extends AppCompatActivity {
    private static final int GALLERY_REQUEST = 1;
    private RecyclerView categoriesRecyclerView;
    private EditText ruAnswer;
    private EditText enAnswer;
    private EditText deAnswer;
    private ImageView questionImage;
    private Button pickButton;
    private Button createButton;
    private Button backButton;
    private Picture image;
    private Category selectedCategory;
    private QuestionStorage storage;
    private int selectedPosition = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question_creation_layout);
        storage = QuestionStorage.getStorage(this);
        selectedCategory = storage.getNotBaseCategories().get(0);
        categoriesRecyclerView = findViewById(R.id.question_creation_categories_recycler_view);
        categoriesRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        categoriesRecyclerView.setAdapter(new CategoryAdapter(storage.getNotBaseCategories(), this));
        ruAnswer = findViewById(R.id.question_creation_russian_editText);
        ruAnswer.setOnKeyListener(new EnterListener(this, ruAnswer));
        enAnswer = findViewById(R.id.question_creation_english_editText);
        enAnswer.setOnKeyListener(new EnterListener(this, enAnswer));
        deAnswer = findViewById(R.id.question_creation_deutsch_editText);
        deAnswer.setOnKeyListener(new EnterListener(this, deAnswer));
        questionImage = findViewById(R.id.question_creation_picture);
        pickButton = findViewById(R.id.question_creation_pick_button);
        pickButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, GALLERY_REQUEST);
            }
        });
        createButton = findViewById(R.id.question_creation_end_button);
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(image != null && !ruAnswer.getText().toString().equals("") && !enAnswer.getText().toString().equals("") && !deAnswer.getText().toString().equals("")) {
                    Question question = new Question(image, ruAnswer.getText().toString(), enAnswer.getText().toString(), deAnswer.getText().toString());
                    selectedCategory.addQuestion(question);
                    storage.add(image);
                    storage.add(question);
                    finish();
                }
                else {
                    if(image == null) {
                        Toast.makeText(QuestionCreationActivity.this, "Выберите картинку", Toast.LENGTH_SHORT).show();
                    }
                    else if(ruAnswer.getText().toString().equals("")){
                        Toast.makeText(QuestionCreationActivity.this, "Заполните ответ на русском", Toast.LENGTH_SHORT).show();
                        //ruAnswer.requestFocus();
                        //InputMethodManager imm = (InputMethodManager)getSystemService(QuestionCreationActivity.INPUT_METHOD_SERVICE);
                        //imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,InputMethodManager.HIDE_IMPLICIT_ONLY);
                        showKeyboard(ruAnswer);
                    }
                    else if(enAnswer.getText().toString().equals("")){
                        //enAnswer.requestFocus();
                        //InputMethodManager imm = (InputMethodManager)getSystemService(QuestionCreationActivity.INPUT_METHOD_SERVICE);
                        //imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,InputMethodManager.HIDE_IMPLICIT_ONLY);
                        showKeyboard(enAnswer);
                    }
                    else if(deAnswer.getText().toString().equals("")){
                        //deAnswer.requestFocus();
                        //InputMethodManager imm = (InputMethodManager)getSystemService(QuestionCreationActivity.INPUT_METHOD_SERVICE);
                        //imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,InputMethodManager.HIDE_IMPLICIT_ONLY);
                        showKeyboard(deAnswer);
                    }
                }
            }
        });
        backButton = findViewById(R.id.question_creation_back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch(requestCode) {
            case GALLERY_REQUEST:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                    try {
                        image = new Picture(selectedImage.toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    questionImage.setImageBitmap(image.getPicture(this));
                }
        }
    }

    private class CategoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView text;
        private ImageView image;
        private Category category;
        private Context context;
        private CategoryAdapter adapter;

        public CategoryHolder(@NonNull View itemView, Context context, CategoryAdapter adapter) {
            super(itemView);
            this.context = context;
            image = itemView.findViewById(R.id.category_item_image);
            text = itemView.findViewById(R.id.category_item_text);
            this.adapter = adapter;
            itemView.setOnClickListener(this);
        }

        public void bindCategory(Category category){
            this.category = category;
            image.setImageBitmap(Pictures.getInstance().getPictures().get(62).getPicture(getApplicationContext()));
            new CategoryImageLoadTask(this, category.getPicture(), context).execute();
            text.setText(category.getCategoryName());
        }

        @Override
        public void onClick(View v) {
            selectedCategory = category;
            Toast.makeText(context, "Выбрана категория " + category.getCategoryName(), Toast.LENGTH_SHORT).show();
            adapter.notifyItemChanged(selectedPosition);
            adapter.notifyItemChanged(getAdapterPosition());
            selectedPosition = getAdapterPosition();
        }
    }

    private class CategoryAdapter extends RecyclerView.Adapter<CategoryHolder> {
        private List<Category> categories;
        private Context context;

        public CategoryAdapter(List<Category> category, Context context) {
            this.categories = category;
            this.context = context;
        }

        @Override
        public CategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = getLayoutInflater();
            View v = inflater.inflate(R.layout.category_item, parent, false);

            return new CategoryHolder(v, context,this);
        }

        @Override
        public void onBindViewHolder(@NonNull CategoryHolder holder, int position) {
            Category category = categories.get(position);
            holder.bindCategory(category);
            if(category.equals(selectedCategory)) {
                holder.itemView.setBackground(context.getDrawable(R.drawable.category_selected_item_border));
            }
            else{
                holder.itemView.setBackground(context.getDrawable(R.drawable.category_item_border));
            }
        }

        @Override
        public int getItemCount() {
            return categories.size();
        }
    }

    public void showKeyboard(View v){
        v.requestFocus();
        InputMethodManager imm = (InputMethodManager)getSystemService(QuestionCreationActivity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

}
