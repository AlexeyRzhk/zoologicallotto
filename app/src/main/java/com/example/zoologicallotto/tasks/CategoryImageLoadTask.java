package com.example.zoologicallotto.tasks;

import android.content.Context;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;


import com.example.zoologicallotto.Picture;
import com.example.zoologicallotto.R;

public class CategoryImageLoadTask extends ImageLoadTask {


    public CategoryImageLoadTask(RecyclerView.ViewHolder holder, Picture picture, Context context) {
        super(holder, picture, context);
    }

    @Override
    protected ImageView getImageContainer(){
        return holder.itemView.findViewById(R.id.category_item_image);
    }
}