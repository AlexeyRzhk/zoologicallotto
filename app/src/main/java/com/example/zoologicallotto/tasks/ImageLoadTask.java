package com.example.zoologicallotto.tasks;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.zoologicallotto.Category;
import com.example.zoologicallotto.Picture;
import com.example.zoologicallotto.R;

public abstract class ImageLoadTask extends AsyncTask<Void, Void, Bitmap> {
    protected RecyclerView.ViewHolder holder;
    protected Picture picture;
    protected Context context;

    public ImageLoadTask(RecyclerView.ViewHolder holder, Picture picture, Context context) {
        this.holder = holder;
        this.picture = picture;
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected Bitmap doInBackground(Void... voids) {
        Bitmap bitmap = picture.getPicture(context);
        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap aBitmap) {
        super.onPostExecute(aBitmap);
        ImageView image = getImageContainer();
        image.setImageBitmap(aBitmap);

    }

    protected abstract ImageView getImageContainer();
}
