package com.example.zoologicallotto.tasks;

import android.content.Context;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.zoologicallotto.Picture;
import com.example.zoologicallotto.R;

public class QuestionImageLoadTask extends ImageLoadTask {
    public QuestionImageLoadTask(RecyclerView.ViewHolder holder, Picture picture, Context context) {
        super(holder, picture, context);
    }

    @Override
    protected ImageView getImageContainer() {
        ImageView image = holder.itemView.findViewById(R.id.question_item_image);
        return image;
    }
}
