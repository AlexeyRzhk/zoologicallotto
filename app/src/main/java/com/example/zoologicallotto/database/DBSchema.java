package com.example.zoologicallotto.database;

public class DBSchema {
    public static final class CategoryTable{
        public static final String NAME = "categories";
        public static final class Columns{
            public static final String PICTURE_ID = "pictureID";
            public static final String CATEGORY_ID = "categoryID";
            public static final String CATEGORY_TEXT = "category_text";
        }
    }
    public static final class QuestionTable{
        public static final String NAME = "questions";
        public static final class Columns{
            public static final String UUID = "uuid";
            public static final String PICTURE_ID = "pictureID";
            public static final String CATEGORY_ID = "categoryID";
            public static final String RUSSIAN_ANSWER = "russian_answer";
            public static final String ENGLISH_ANSWER = "english_answer";
            public static final String DEUTSCH_ANSWER = "deutsch_answer";
        }
    }
    public static final class PictureTable {
        public static final String NAME = "pictures";
        public static final class Columns {
            public static final String PICTURE_ID = "pictureID";
            public static final String PICTURE_PATH = "picture_path";
            public static final String UUID = "uuid";
        }
    }

}
