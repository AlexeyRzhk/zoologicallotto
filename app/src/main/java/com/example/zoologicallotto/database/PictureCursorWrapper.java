package com.example.zoologicallotto.database;

import android.database.Cursor;
import android.database.CursorWrapper;

import com.example.zoologicallotto.Picture;

public class PictureCursorWrapper extends CursorWrapper {
    public PictureCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public Picture getPicture(){
        int pictureUUID = Integer.parseInt(getString(getColumnIndex(DBSchema.PictureTable.Columns.UUID)));
        int pictureID = Integer.parseInt(getString(getColumnIndex(DBSchema.PictureTable.Columns.PICTURE_ID)));
        String picturePath = getString(getColumnIndex(DBSchema.PictureTable.Columns.PICTURE_PATH));
        return new Picture(picturePath, pictureID, pictureUUID);
    }
}
