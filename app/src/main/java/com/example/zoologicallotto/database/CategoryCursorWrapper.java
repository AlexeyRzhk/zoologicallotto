package com.example.zoologicallotto.database;

import android.database.Cursor;
import android.database.CursorWrapper;

import com.example.zoologicallotto.Category;
import com.example.zoologicallotto.Picture;
import com.example.zoologicallotto.storages.Pictures;

public class CategoryCursorWrapper extends CursorWrapper {
    public CategoryCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public Category getCategory(){
        String categoryName = getString(getColumnIndex(DBSchema.CategoryTable.Columns.CATEGORY_TEXT));
        int categoryID = Integer.parseInt(getString(getColumnIndex(DBSchema.CategoryTable.Columns.CATEGORY_ID)));
        int pictureID = Integer.parseInt(getString(getColumnIndex(DBSchema.CategoryTable.Columns.PICTURE_ID)));
        Picture picture = Pictures.getInstance().getPicture(pictureID);
        if(picture == null){
            picture = new Picture();
        }
        return new Category(picture, categoryName, categoryID);
    }
}
