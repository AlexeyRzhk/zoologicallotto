package com.example.zoologicallotto.database;
import com.example.zoologicallotto.Category;
import com.example.zoologicallotto.Converter;
import com.example.zoologicallotto.Picture;
import com.example.zoologicallotto.Question;
import com.example.zoologicallotto.R;
import com.example.zoologicallotto.database.DBSchema.CategoryTable;
import com.example.zoologicallotto.database.DBSchema.PictureTable;
import com.example.zoologicallotto.database.DBSchema.QuestionTable;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final int VERSION = 1;
    private static final String DATABASE_NAME = "QuestionBase.db";


    public DatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + QuestionTable.NAME + "(" + "_id integer primary key autoincrement, " + QuestionTable.Columns.CATEGORY_ID + ", " + QuestionTable.Columns.PICTURE_ID + ", " + QuestionTable.Columns.RUSSIAN_ANSWER + ", " + QuestionTable.Columns.ENGLISH_ANSWER + ", " + QuestionTable.Columns.DEUTSCH_ANSWER + ", " + QuestionTable.Columns.UUID + ")");
        db.execSQL("create table " + CategoryTable.NAME + "(" + "_id integer primary key autoincrement, " + CategoryTable.Columns.CATEGORY_ID + ", " + CategoryTable.Columns.PICTURE_ID + ", " + CategoryTable.Columns.CATEGORY_TEXT + ")");
        db.execSQL("create table " + PictureTable.NAME + "(" + "_id integer primary key autoincrement, " +  PictureTable.Columns.PICTURE_PATH + ", " + PictureTable.Columns.PICTURE_ID + ", " + PictureTable.Columns.UUID + ")");

        List<Category> categories = new ArrayList<>();
        List<Picture> pictures = new ArrayList<>();
        //time of year
        pictures.add(new Picture(R.mipmap.autumn));//0
        pictures.add(new Picture(R.mipmap.summer));//1
        pictures.add(new Picture(R.mipmap.winter));//2
        pictures.add(new Picture(R.mipmap.spring));//3
        //fruits
        pictures.add(new Picture(R.mipmap.apple));//4
        pictures.add(new Picture(R.mipmap.cherries));//5
        pictures.add(new Picture(R.mipmap.mango));//6
        pictures.add(new Picture(R.mipmap.peach));//7
        pictures.add(new Picture(R.mipmap.plum));//8
        pictures.add(new Picture(R.mipmap.watermelon));//9
        //animals
        pictures.add(new Picture(R.mipmap.arcticfox));//10
        pictures.add(new Picture(R.mipmap.bear));//11
        pictures.add(new Picture(R.mipmap.elephant));//12
        pictures.add(new Picture(R.mipmap.fox));//13
        pictures.add(new Picture(R.mipmap.giraffe));//14
        pictures.add(new Picture(R.mipmap.hedgehog));//15
        pictures.add(new Picture(R.mipmap.lion));//16
        pictures.add(new Picture(R.mipmap.lynx));//17
        pictures.add(new Picture(R.mipmap.mammal));//18
        pictures.add(new Picture(R.mipmap.rabbit));//19
        pictures.add(new Picture(R.mipmap.robbe));//20
        pictures.add(new Picture(R.mipmap.tiger));//21
        pictures.add(new Picture(R.mipmap.wolf));//22
        pictures.add(new Picture(R.mipmap.zebra));//23
        pictures.add(new Picture(R.mipmap.fruits));//24
        pictures.add(new Picture(R.mipmap.seasons));//25
        pictures.add(new Picture(R.mipmap.great));//26
        pictures.add(new Picture(R.mipmap.bear));//27
        //fruits
        pictures.add(new Picture(R.mipmap.banana));//28
        pictures.add(new Picture(R.mipmap.kiwi));//29
        pictures.add(new Picture(R.mipmap.lemon));//30
        pictures.add(new Picture(R.mipmap.orange));//31
        pictures.add(new Picture(R.mipmap.peach));//32
        pictures.add(new Picture(R.mipmap.pear));//33
        pictures.add(new Picture(R.mipmap.pineapple));//34
        pictures.add(new Picture(R.mipmap.plum));//35
        pictures.add(new Picture(R.mipmap.pomegranate));//36
        pictures.add(new Picture(R.mipmap.tangerine));//37
        //vegetables
        pictures.add(new Picture(R.mipmap.vegetablespicture));//38
        pictures.add(new Picture(R.mipmap.carrot));//39
        pictures.add(new Picture(R.mipmap.corn));//40
        pictures.add(new Picture(R.mipmap.cucumber));//41
        pictures.add(new Picture(R.mipmap.eggplant));//42
        pictures.add(new Picture(R.mipmap.garlic));//43
        pictures.add(new Picture(R.mipmap.ginger));//44
        pictures.add(new Picture(R.mipmap.onion));//45
        pictures.add(new Picture(R.mipmap.pepper));//46
        pictures.add(new Picture(R.mipmap.potato));//47
        pictures.add(new Picture(R.mipmap.pumpkin));//48
        pictures.add(new Picture(R.mipmap.shroom));//49
        pictures.add(new Picture(R.mipmap.cabbage));//50
        pictures.add(new Picture(R.mipmap.tomato));//51
        //berries
        pictures.add(new Picture(R.mipmap.berries));//52
        pictures.add(new Picture(R.mipmap.blackberries));//53
        pictures.add(new Picture(R.mipmap.blueberrie));//54
        pictures.add(new Picture(R.mipmap.cherry));//55
        pictures.add(new Picture(R.mipmap.currant));//56
        pictures.add(new Picture(R.mipmap.gooseberry));//57
        pictures.add(new Picture(R.mipmap.grapes));//58
        pictures.add(new Picture(R.mipmap.raspberry));//59
        pictures.add(new Picture(R.mipmap.rowan));//60
        pictures.add(new Picture(R.mipmap.strawberries));//61

        pictures.add(new Picture(R.mipmap.loading));//62
        //transport
        pictures.add(new Picture(R.mipmap.highway));//63
        pictures.add(new Picture(R.mipmap.airplane));//64
        pictures.add(new Picture(R.mipmap.boat));//65
        pictures.add(new Picture(R.mipmap.bus));//66
        pictures.add(new Picture(R.mipmap.car));//67
        pictures.add(new Picture(R.mipmap.motorcycle));//68
        pictures.add(new Picture(R.mipmap.ship));//69
        pictures.add(new Picture(R.mipmap.train));//70
        pictures.add(new Picture(R.mipmap.tram));//71
        pictures.add(new Picture(R.mipmap.uerdingen));//72
        //colors
        pictures.add(new Picture(R.mipmap.colors));//73
        pictures.add(new Picture(R.mipmap.blue));//74
        pictures.add(new Picture(R.mipmap.brown));//75
        pictures.add(new Picture(R.mipmap.black));//76
        pictures.add(new Picture(R.mipmap.green));//77
        pictures.add(new Picture(R.mipmap.grey));//78
        pictures.add(new Picture(R.mipmap.orange_color));//79
        pictures.add(new Picture(R.mipmap.pink));//80
        pictures.add(new Picture(R.mipmap.red));//81
        pictures.add(new Picture(R.mipmap.violet));//82
        pictures.add(new Picture(R.mipmap.yellow));//83


        categories.add(new Category("времена года", pictures.get(25)));
        categories.add(new Category("животные", pictures.get(27)));
        categories.add(new Category("фрукты", pictures.get(24)));
        categories.add(new Category("овощи", pictures.get(38)));
        categories.add(new Category("ягоды", pictures.get(52)));
        categories.add(new Category("транспорт", pictures.get(63)));
        categories.add(new Category("цвета", pictures.get(73)));


        //time of year
        categories.get(0).addQuestion(new Question(pictures.get(0), "осень", "autumn", "fallen"));
        categories.get(0).addQuestion(new Question(pictures.get(1), "лето", "summer", "sommer"));
        categories.get(0).addQuestion(new Question(pictures.get(2), "зима", "winter", "winter"));
        categories.get(0).addQuestion(new Question(pictures.get(3), "весна", "spring", "Frühling"));
        //animals
        categories.get(1).addQuestion(new Question(pictures.get(10), "песец", "arctic fox", "Fuchs"));
        categories.get(1).addQuestion(new Question(pictures.get(11), "медведь", "bear", "Bär"));
        categories.get(1).addQuestion(new Question(pictures.get(12), "слон", "elephant", "elefant"));
        categories.get(1).addQuestion(new Question(pictures.get(13), "лиса", "fox", "Fuchs"));
        categories.get(1).addQuestion(new Question(pictures.get(14), "жираф", "giraffe", "giraffe"));
        categories.get(1).addQuestion(new Question(pictures.get(15), "ёж", "hedgehog", "igel"));
        categories.get(1).addQuestion(new Question(pictures.get(16), "лев", "lion", "löwe"));
        categories.get(1).addQuestion(new Question(pictures.get(17), "рысь", "lynx", "Luchs"));
        categories.get(1).addQuestion(new Question(pictures.get(18), "кабан", "boar", "Eber"));
        categories.get(1).addQuestion(new Question(pictures.get(19), "заяц", "hare", "hase"));
        categories.get(1).addQuestion(new Question(pictures.get(20), "тюлень", "seal", "dichtung"));
        categories.get(1).addQuestion(new Question(pictures.get(21), "тигр", "tiger", "tiger"));
        categories.get(1).addQuestion(new Question(pictures.get(22), "волк", "wolf", "wolf"));
        categories.get(1).addQuestion(new Question(pictures.get(23), "зебра", "zebra", "zebra"));
        //fruits
        categories.get(2).addQuestion(new Question(pictures.get(4), "яблоко", "apple", "apfel"));
        categories.get(2).addQuestion(new Question(pictures.get(5), "вишня", "cherry", "Kirsche"));
        categories.get(2).addQuestion(new Question(pictures.get(6), "манго", "mango", "mango"));
        categories.get(2).addQuestion(new Question(pictures.get(7), "персик", "peach", "pfirsich"));
        categories.get(2).addQuestion(new Question(pictures.get(8), "слива", "plum", "Pflaume"));
        categories.get(2).addQuestion(new Question(pictures.get(9), "арбуз", "watermelon", "wassermelone"));
        categories.get(2).addQuestion(new Question(pictures.get(28), "банан", "banana", "Banane"));
        categories.get(2).addQuestion(new Question(pictures.get(29), "киви", "kiwi", "Kiwi"));
        categories.get(2).addQuestion(new Question(pictures.get(30), "лимон", "lemon", "Zitrone"));
        categories.get(2).addQuestion(new Question(pictures.get(31), "апельсин", "orange", "Orange"));
        categories.get(2).addQuestion(new Question(pictures.get(32), "персик", "peach", "Pfirsich"));
        categories.get(2).addQuestion(new Question(pictures.get(33), "груша", "pear", "Birne"));
        categories.get(2).addQuestion(new Question(pictures.get(34), "ананас", "pineapple", "Ananas"));
        categories.get(2).addQuestion(new Question(pictures.get(36), "гранат", "pomegranate", "Granatapfel"));
        categories.get(2).addQuestion(new Question(pictures.get(37), "мандарин", "tangerine", "Mandarine"));
        //vegetables
        categories.get(3).addQuestion(new Question(pictures.get(39), "морковь", "carrot", "Karotte"));
        categories.get(3).addQuestion(new Question(pictures.get(40), "кукуруза", "corn", "Mais"));
        categories.get(3).addQuestion(new Question(pictures.get(41), "огурец", "cucumber", "Gurke"));
        categories.get(3).addQuestion(new Question(pictures.get(42), "баклажан", "eggplant", "Aubergine"));
        categories.get(3).addQuestion(new Question(pictures.get(43), "чеснок", "garlic", "Knoblauch"));
        categories.get(3).addQuestion(new Question(pictures.get(44), "имбирь", "ginger", "Ingwer"));
        categories.get(3).addQuestion(new Question(pictures.get(45), "лук", "onion", "Zwiebel"));
        categories.get(3).addQuestion(new Question(pictures.get(46), "перец", "pepper", "Pfeffer"));
        categories.get(3).addQuestion(new Question(pictures.get(47), "картофель", "potato", "Kartoffel"));
        categories.get(3).addQuestion(new Question(pictures.get(48), "тыква", "pumpkin", "Kürbis"));
        categories.get(3).addQuestion(new Question(pictures.get(49), "гриб", "mushroom", "Pilz"));
        categories.get(3).addQuestion(new Question(pictures.get(50), "капуста", "cabbage", "Kohl"));
        categories.get(3).addQuestion(new Question(pictures.get(51), "помидор", "tomato", "Tomate"));
        //berries
        categories.get(4).addQuestion(new Question(pictures.get(53), "ежевика", "blackberry", "Brombeere"));
        categories.get(4).addQuestion(new Question(pictures.get(54), "черника", "blueberry", "Blaubeere"));
        categories.get(4).addQuestion(new Question(pictures.get(55), "вишня", "cherry", "Kirsche"));
        categories.get(4).addQuestion(new Question(pictures.get(56), "смородина", "currant", "Johannisbeere"));
        categories.get(4).addQuestion(new Question(pictures.get(57), "крыжовник", "gooseberry", "Stachelbeere"));
        categories.get(4).addQuestion(new Question(pictures.get(58), "виноград", "grapes", "Trauben"));
        categories.get(4).addQuestion(new Question(pictures.get(59), "малина", "raspberry", "Himbeere"));
        categories.get(4).addQuestion(new Question(pictures.get(60), "рябина", "rowan", "Eberesche"));
        categories.get(4).addQuestion(new Question(pictures.get(61), "клубника", "strawberries", "Erdbeeren"));
        //transport
        categories.get(5).addQuestion(new Question(pictures.get(64), "самолет", "airplane", "Flugzeug"));
        categories.get(5).addQuestion(new Question(pictures.get(65), "катер", "boat", "Boot"));
        categories.get(5).addQuestion(new Question(pictures.get(66), "автобус", "bus", "bus"));
        categories.get(5).addQuestion(new Question(pictures.get(67), "машина", "car", "auto"));
        categories.get(5).addQuestion(new Question(pictures.get(68), "мотоцикл", "motorcycle", "Motorrad"));
        categories.get(5).addQuestion(new Question(pictures.get(69), "корабль", "ship", "Schiff"));
        categories.get(5).addQuestion(new Question(pictures.get(70), "поезд", "train", "Zug"));
        categories.get(5).addQuestion(new Question(pictures.get(71), "трамвай", "tram", "Straßenbahn"));
        categories.get(5).addQuestion(new Question(pictures.get(72), "троллейбус", "trolleybus", "Obus"));
        //colors
        categories.get(6).addQuestion(new Question(pictures.get(74), "голубой", "blue", "Blau"));
        categories.get(6).addQuestion(new Question(pictures.get(75), "коричневый", "brown", "braun"));
        categories.get(6).addQuestion(new Question(pictures.get(76), "черный", "black", "schwarz"));
        categories.get(6).addQuestion(new Question(pictures.get(77), "зеленый", "green", "Grün"));
        categories.get(6).addQuestion(new Question(pictures.get(78), "серый", "grey", "grau"));
        categories.get(6).addQuestion(new Question(pictures.get(79), "оранжевый", "orange", "Orange"));
        categories.get(6).addQuestion(new Question(pictures.get(80), "розовый", "pink", "Rosa"));
        categories.get(6).addQuestion(new Question(pictures.get(81), "красный", "red", "rot"));
        categories.get(6).addQuestion(new Question(pictures.get(82), "фиолетовый", "violet", "violett"));
        categories.get(6).addQuestion(new Question(pictures.get(83), "желтый", "yellow", "Gelb"));





        for(Picture picture: pictures){
            db.insert(PictureTable.NAME, null, Converter.pictureConvert(picture));
        }
        for(Category category: categories){
            for(Question question: category.getQuestions()){
                db.insert(QuestionTable.NAME, null, Converter.questionConvert(question));
            }
            db.insert(CategoryTable.NAME, null, Converter.categoryConvert(category));
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }




}
