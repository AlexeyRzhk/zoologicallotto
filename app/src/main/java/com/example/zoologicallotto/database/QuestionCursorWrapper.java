package com.example.zoologicallotto.database;

import android.database.Cursor;
import android.database.CursorWrapper;

import com.example.zoologicallotto.Picture;
import com.example.zoologicallotto.storages.Pictures;
import com.example.zoologicallotto.Question;
import com.example.zoologicallotto.database.DBSchema.QuestionTable.Columns;

public class QuestionCursorWrapper extends CursorWrapper {
    public QuestionCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public Question getQuestion(){
        int questionID = Integer.parseInt(getString(getColumnIndex(Columns.UUID)));
        int categoryID = Integer.parseInt(getString(getColumnIndex(Columns.CATEGORY_ID)));
        String rAnswer = getString(getColumnIndex(Columns.RUSSIAN_ANSWER));
        String eAnswer = getString(getColumnIndex(Columns.ENGLISH_ANSWER));
        String dAnswer = getString(getColumnIndex(Columns.DEUTSCH_ANSWER));
        int pictureID = Integer.parseInt(getString(getColumnIndex(Columns.PICTURE_ID)));
        Picture picture = Pictures.getInstance().getPicture(pictureID);
        if(picture == null){
            picture = new Picture();
        }
        return new Question(picture, rAnswer, eAnswer, dAnswer, questionID, categoryID);
    }
}
