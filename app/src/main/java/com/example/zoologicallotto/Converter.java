package com.example.zoologicallotto;

import android.content.ContentValues;

import com.example.zoologicallotto.database.DBSchema;

public class Converter {

    public static ContentValues questionConvert(Question question){
        ContentValues values = new ContentValues();
        values.put(DBSchema.QuestionTable.Columns.CATEGORY_ID, question.getCategoryID());
        values.put(DBSchema.QuestionTable.Columns.DEUTSCH_ANSWER, question.getDeutschAnswer());
        values.put(DBSchema.QuestionTable.Columns.ENGLISH_ANSWER, question.getEnglishAnswer());
        values.put(DBSchema.QuestionTable.Columns.RUSSIAN_ANSWER, question.getRussianAnswer());
        values.put(DBSchema.QuestionTable.Columns.PICTURE_ID, question.getPicture().getUuid());
        values.put(DBSchema.QuestionTable.Columns.UUID, question.getQuestionID());
        return values;
    }

    public static ContentValues categoryConvert(Category category){
        ContentValues values = new ContentValues();
        values.put(DBSchema.CategoryTable.Columns.CATEGORY_ID, category.getCategoryID());
        values.put(DBSchema.CategoryTable.Columns.CATEGORY_TEXT, category.getCategoryName());
        values.put(DBSchema.CategoryTable.Columns.PICTURE_ID, category.getPicture().getUuid());
        return values;
    }

    public static ContentValues pictureConvert(Picture picture){
        ContentValues values = new ContentValues();
        values.put(DBSchema.PictureTable.Columns.PICTURE_ID, picture.getPictureID());
        values.put(DBSchema.PictureTable.Columns.PICTURE_PATH, picture.getPath());
        values.put(DBSchema.PictureTable.Columns.UUID, picture.getUuid());
        return values;
    }
}
