package com.example.zoologicallotto;

import android.content.Context;
import android.graphics.Bitmap;

public class Question {
    private static int number = 0;
    private Picture picture;
    private String russianAnswer;
    private String englishAnswer;
    private String deutschAnswer;
    private int questionID;
    private int categoryID;
    private boolean isBase;

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

    public Picture getPicture() {
        return picture;
    }

    public int getQuestionID() {
        return questionID;
    }

    public boolean isBase() {
        return isBase;
    }

    public int getCategoryID() {
        return categoryID;
    }

    public String getRussianAnswer() {
        return russianAnswer;
    }

    public String getEnglishAnswer() {
        return englishAnswer;
    }

    public String getDeutschAnswer() {
        return deutschAnswer;
    }

    public Bitmap getPicture(Context context){
        return picture.getPicture(context);
    }

    public static void setNumber(int number) {
        Question.number = number;
    }

    public Question(Picture picture, String russianAnswer, String englishAnswer, String deutschAnswer) {
        isBase = picture.getPictureID() != 0;
        this.picture = picture;
        this.russianAnswer = russianAnswer;
        this.englishAnswer = englishAnswer;
        this.deutschAnswer = deutschAnswer;
        questionID = number;
        number++;
    }



    public Question(Picture picture, String russianAnswer, String englishAnswer, String deutschAnswer, int questionID, int categoryID) {
        isBase = picture.getPictureID() != 0;
        this.picture = picture;
        this.russianAnswer = russianAnswer;
        this.englishAnswer = englishAnswer;
        this.deutschAnswer = deutschAnswer;
        this.questionID = questionID;
        this.categoryID = categoryID;
    }
}
