package com.example.zoologicallotto.dialogs;

import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

public class CustomDialog extends DialogFragment {
    private String message;

    public CustomDialog(String message) {
        this.message = message;
    }

    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        return builder
                .setTitle("Внимание")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage(message)
                .setNeutralButton("OK", null)
                .create();
    }
}