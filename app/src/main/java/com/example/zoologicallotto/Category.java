package com.example.zoologicallotto;

import java.util.ArrayList;
import java.util.List;

public class Category {
    private List<Question> questions;
    private Picture picture;
    private String categoryName;
    private int categoryID;
    private boolean isBase;
    private static int numberOfCategories = 0;
    private static final int PICTURE_SIZE = 210;

    public static int getPictureSize() {
        return PICTURE_SIZE;
    }

    public int getCategoryID() {
        return categoryID;
    }

    public Picture getPicture() {
        return picture;
    }

    public boolean isBase() {
        return isBase;
    }

    public void addQuestion(Question question){
        question.setCategoryID(categoryID);
        questions.add(question);
    }
    public void deleteQuestion(Question question){
        questions.remove(question);
    }

    public static void setNumberOfCategories(int numberOfCategories) {
        Category.numberOfCategories = numberOfCategories;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public Category(String categoryName, Picture picture) {
        isBase = picture.getPictureID() != 0;
        this.questions = new ArrayList<>();
        this.picture = picture;
        this.categoryName = categoryName;
        categoryID = numberOfCategories;
        numberOfCategories++;
    }

    public Category(Picture picture, String categoryName, int categoryID) {
        isBase = picture.getPictureID() != 0;
        this.questions = new ArrayList<>();
        this.picture = picture;
        this.categoryName = categoryName;
        this.categoryID = categoryID;
        numberOfCategories++;
    }

    public Category(String categoryName) {
        this.questions = new ArrayList<>();
        this.categoryName = categoryName;
    }

    public String getCategoryName() {
        return categoryName;
    }
}
