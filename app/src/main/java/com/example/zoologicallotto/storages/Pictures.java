package com.example.zoologicallotto.storages;

import com.example.zoologicallotto.Picture;

import java.util.ArrayList;
import java.util.List;

public class Pictures {
    private static Pictures pictures;
    private List<Picture> listOfPictures;

    public Pictures() {
        listOfPictures = new ArrayList<>();
    }

    public static Pictures getInstance(){
        if(pictures == null){
            pictures = new Pictures();
        }
        return pictures;
    }

    public List<Picture> getPictures(){
        return listOfPictures;
    }

    public Picture getPicture(int pictureUUID){
        for(Picture picture: listOfPictures){
            if(picture.getUuid() == pictureUUID){
                return picture;
            }
        }
        return null;
    }
}
