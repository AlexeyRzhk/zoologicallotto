package com.example.zoologicallotto.storages;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.zoologicallotto.Category;
import com.example.zoologicallotto.Converter;
import com.example.zoologicallotto.Picture;
import com.example.zoologicallotto.Question;
import com.example.zoologicallotto.database.CategoryCursorWrapper;
import com.example.zoologicallotto.database.DatabaseHelper;
import com.example.zoologicallotto.database.PictureCursorWrapper;
import com.example.zoologicallotto.database.QuestionCursorWrapper;

import java.util.ArrayList;
import java.util.List;

import static com.example.zoologicallotto.database.DBSchema.*;

public class QuestionStorage {
    private static QuestionStorage storage;

    private ArrayList<Category> categories;
    private Context context;
    private SQLiteDatabase database;

    private QuestionStorage(Context context) {
        categories = new ArrayList<>();
        this.context = context;
        database = new DatabaseHelper(this.context).getWritableDatabase();
        update();
    }

    public static QuestionStorage getStorage(Context context) {
        if(storage == null){
            storage = new QuestionStorage(context);
        }
        return storage;
    }

    public ArrayList<Category> getCategories() {
        return categories;
    }

    public Category getCategory(int categoryID){
        for(Category category: categories){
            if(category.getCategoryID() == categoryID){
                return category;
            }
        }
        return null;
    }

    private QuestionCursorWrapper queryQuestions(String whereClause, String[] whereArgs) {
        Cursor cursor = database.query(
                QuestionTable.NAME,
                null, // Columns - null выбирает все столбцы
                whereClause,
                whereArgs,
                null, // groupBy
                null, // having
                null // orderBy
        );
        return new QuestionCursorWrapper(cursor);
    }

    private CategoryCursorWrapper queryCategories(String whereClause, String[] whereArgs) {
        Cursor cursor = database.query(
                CategoryTable.NAME,
                null, // Columns - null выбирает все столбцы
                whereClause,
                whereArgs,
                null, // groupBy
                null, // having
                null // orderBy
        );
        return new CategoryCursorWrapper(cursor);
    }

    private PictureCursorWrapper queryPictures(String whereClause, String[] whereArgs) {
        Cursor cursor = database.query(
                PictureTable.NAME,
                null, // Columns - null выбирает все столбцы
                whereClause,
                whereArgs,
                null, // groupBy
                null, // having
                null // orderBy
        );
        return new PictureCursorWrapper(cursor);
    }

    public void update(){
        List<Picture> pictures = Pictures.getInstance().getPictures();
        pictures.clear();
        for(Category category: categories){
            category.getQuestions().clear();
        }
        categories.clear();
        PictureCursorWrapper pictureCursor = queryPictures(null, null);
        CategoryCursorWrapper categoryCursor = queryCategories(null, null);
        QuestionCursorWrapper questionCursor = queryQuestions(null, null);
        try {
            pictureCursor.moveToFirst();
            while (!pictureCursor.isAfterLast()) {
                pictures.add(pictureCursor.getPicture());
                pictureCursor.moveToNext();
            }
        }
        finally {
            pictureCursor.close();
        }
        try {
            categoryCursor.moveToFirst();
            while (!categoryCursor.isAfterLast()) {
                categories.add(categoryCursor.getCategory());
                categoryCursor.moveToNext();
            }
        }
        finally {
            categoryCursor.close();
        }
        ArrayList<Question> questions = new ArrayList<>();
        try {
            questionCursor.moveToFirst();
            while (!questionCursor.isAfterLast()) {
                questions.add(questionCursor.getQuestion());
                questionCursor.moveToNext();
            }
        }
        finally {
            questionCursor.close();
        }
        for(Question question: questions){
            getCategory(question.getCategoryID()).addQuestion(question);
        }
        Question.setNumber(questions.get(questions.size() - 1).getQuestionID() + 1);
        Picture.setNumber(pictures.get(pictures.size() - 1).getUuid() + 1);
        Category.setNumberOfCategories(categories.get(categories.size() - 1).getCategoryID() + 1);

    }

    public void add(Picture picture){
        addPicture(picture);
    }
    public void add(Question question){
        addQuestion(question);
    }
    public void add(Category category){
        addCategory(category);
    }

    private void addPicture(Picture picture){
        database.insert(PictureTable.NAME, null, Converter.pictureConvert(picture));
    }
    private void addQuestion(Question question){
        database.insert(QuestionTable.NAME, null, Converter.questionConvert(question));
    }
    private void addCategory(Category category){
        database.insert(CategoryTable.NAME, null, Converter.categoryConvert(category));
    }

    public void delete(Question question){
        deleteQuestion(question);
    }
    public void delete(Category category){
        deleteCategory(category);
    }
    private void deleteQuestion(Question question){
        getCategory(question.getCategoryID()).deleteQuestion(question);
        database.delete(QuestionTable.NAME, QuestionTable.Columns.UUID + " =  ?", new String[]{Integer.toString(question.getQuestionID())});
    }
    private void deleteCategory(Category category){
        categories.remove(category);
        category.getQuestions().clear();
        database.execSQL("DELETE FROM " + QuestionTable.NAME + " WHERE " + QuestionTable.Columns.CATEGORY_ID + " = " + category.getCategoryID() + ";");
        database.execSQL("DELETE FROM " + CategoryTable.NAME + " WHERE " + CategoryTable.Columns.CATEGORY_ID + " = " + category.getCategoryID() + ";");
    }

    public List<Question> getNotBaseQuestionsInCategory(Category category){
        ArrayList<Question> notBaseQuestions = new ArrayList<>();
        for (Question question: category.getQuestions()){
            if(!question.isBase()){
                notBaseQuestions.add(question);
            }
        }
        return notBaseQuestions;
    }

    public int getNumberOfUserQuestions(){
        int count = 0;
        for(Category category: categories){
            count += getNotBaseQuestionsInCategory(category).size();
        }
        return count;
    }

    public List<Category> getNotBaseCategories(){
        ArrayList<Category> notBaseCategories = new ArrayList<>();
        for(Category category: categories){
            if(!category.isBase()){
                notBaseCategories.add(category);
            }
        }
        return notBaseCategories;
    }

}
