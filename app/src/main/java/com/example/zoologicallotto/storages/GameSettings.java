package com.example.zoologicallotto.storages;

import android.content.Context;

import com.example.zoologicallotto.Category;
import com.example.zoologicallotto.R;

public class GameSettings {
    private static GameSettings settings;

    private boolean gameType;
    private Category chosenCategory;
    private String chosenLanguage;
    private Context context;

    public String getChosenLanguage() {
        return chosenLanguage;
    }

    public void setChosenLanguage(String chosenLanguage) {
        this.chosenLanguage = chosenLanguage;
    }

    private GameSettings(Context context) {
        gameType = false;
        this.context = context;
        chosenCategory = QuestionStorage.getStorage(this.context).getCategories().get(0);
        chosenLanguage = context.getString(R.string.russian);
    }

    public boolean isGameType() {
        return gameType;
    }

    public void setGameType(boolean gameType) {
        this.gameType = gameType;
    }

    public static GameSettings getSettings(Context context) {
        if(settings == null){
            settings = new GameSettings(context);
        }
        return settings;
    }

    public Category getChosenCategory() {
        return chosenCategory;
    }

    public void setChosenCategory(Category chosenCategory) {
        this.chosenCategory = chosenCategory;
    }

}
